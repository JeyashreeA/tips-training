/*global console:true*/
var a1 = 6;
function add(a) {                     //closure function
    var a1 = a;
    console.log("within closure func: "+ a1);
    return function (b) {             //function returned by closure
        a1 = 4;
        console.log("within returned function: "+a1);
        console.log(a1+b);
    };   
}

var add_out = add(3);

add_out(4);            //calling a closure function
console.log("global a1: "+a1);


//As per what i have seen the closure's variables, though accessible inside the returned function, their values cant be changed from within the
//returned function. Like here, though I have changed the value of a1 within the returned function, 
//it doesnt affect the closure function's variable.
