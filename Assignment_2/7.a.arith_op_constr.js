/*global console:true*/
var subtract = function(a,b) {
        this.a = a;
        this.b = b;
    };

subtract.prototype.sub = function() {
    return this.a - this.b;
};   

var subtractDisp = new subtract(3,1);

console.log("Constructor obj: ");
console.log(subtractDisp);

console.log("constructor calling prototype: " + subtractDisp.sub());