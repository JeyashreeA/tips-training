/*global console:true,describe:true,it:true,require:true, module:true*/
var assert = require("assert");
var assign = require("./12.Num_inherit_func.js");

describe('12.Num_inherit_func.js', function(){
  describe('#method()', function(){
    it('should inherit the function method()', function(){
      assert.strictEqual("This is a inherited method called by 5", (assign.method({"name" : 5})));
    });
  });
});

//describe('12.Num_inherit_func.js', function(){
//  describe('#inherit_proto()', function(){
//    it('should override the parent function draw()', function(){
//      //assert.deepEqual([0,1,1,2], assign.fibonacci_output(4));
//      var value = assign.draw();
//      assert(value === "drawing a square");
//    });
//  });
//});