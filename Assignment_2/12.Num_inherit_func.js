/*global console:true,describe:true,it:true,require:true, module:true*/
var func = function(spec) {
    var that = {};
    that.methodFunc = function(spec) {
        console.log("This is a inherited method called by " + spec.name);
        return "This is a inherited method called by " + spec.name;
    };
    return that;
};

Number.prototype.method = func();

var num = 5;

num.method.methodFunc({"name" : num});

String.prototype.method = func();

var str = "calling";

str.method.methodFunc({"name" : str});

module.exports.method = Number.prototype.method.methodFunc;