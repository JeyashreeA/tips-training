var assert = require("assert");
var assign = require("./19.stack_arr.js");

describe('stack_arr.js', function(){
  describe('#stack_arr()', function(){
    it('should push elements into array', function(){
      //assert.deepEqual([0,1,1,2], assign.fibonacci_output(4));
      //var value = assign.push();
      assert.deepEqual([2],assign.push(2));
    });
  });
});

describe('stack_arr.js', function(){
  describe('#stack_arr()', function(){
    it('should pop elements into array', function(){
      //assert.deepEqual([0,1,1,2], assign.fibonacci_output(4));
      //var value = assign.push();
      assert.deepEqual([],assign.pop());
    });
  });
});