var camera = {
    "model_name" : "Nikon",
    "shutter" : "open",
    "pixels" : 200,
    "toggleShutter" : function () {
        this.shutter = this.shutter?false:true;
    }
};