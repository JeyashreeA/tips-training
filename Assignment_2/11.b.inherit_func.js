/*global console:true,describe:true,it:true,require:true, module:true*/
var lineFunction = function(spec) {
    //console.log(spec.length);
    var that = {};
    that.get_length = function(spec) {
    return spec.length;
};

    that.draw = function() {
        console.log("drawing a shape");
};
    return that;
};

//---------------------------------------------------------Line function------------------------------------------------------------//

var squareFunction = function(spec) {
    spec.length = 4;
    var that = lineFunction(spec);
    that.area = function(length) {
        return spec.length * spec.length;
    };

    that.get_angle = function() {
        console.log("angle is 90");
    };
    return (that);
};

var mySquare = squareFunction({});

mySquare.draw = function() {
        console.log("drawing a square");
    };

mySquare.draw();
mySquare.get_angle();
console.log("Area: " + mySquare.area(5));

//---------------------------------------------------------Square function------------------------------------------------------------//

var rectFunction = function(spec) {
    spec.length = 4;
    var that = lineFunction(spec);
    that.area = function() {
        return spec.length * spec.breadth;
    };
   return (that);
};

var myRect = rectFunction({"breadth" : 5});

myRect.draw = function() {
        console.log("drawing a rectangle");
    };

myRect.draw();
console.log("Area: " + myRect.area(5));

//---------------------------------------------------------Square function------------------------------------------------------------//

var rhombusFunction = function(spec) {
    spec.diag1 = 4;
    var that = squareFunction(spec);
    that.get_angle = function() {
        console.log("angle is usually acute");
    };
    that.area = function() {
        return ((spec.diag1 * spec.diag2)/2);
    };
    return (that);
};

var myRhombus = rhombusFunction({"diag2" : 5});

myRhombus.draw = function() {
        console.log("drawing a rhombus");
    };

myRhombus.draw();
myRhombus.get_angle();
console.log("Area: " + myRhombus.area());

module.exports.draw = mySquare.draw;
module.exports.area = myRhombus.area;