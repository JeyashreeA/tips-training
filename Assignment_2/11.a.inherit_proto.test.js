/*global console:true,describe:true,it:true,require:true*/
var assert = require("assert");
var assign = require("./11.a.inherit_proto.js");

describe('inherit_proto.js', function(){
  describe('#inherit_proto()', function(){
    it('should override the parent function draw()', function(){
      //assert.deepEqual([0,1,1,2], assign.fibonacci_output(4));
      var value = assign.draw();
      assert(value === "drawing a square");
    });
  });
});

describe('inherit_proto.js', function(){
  describe('#area()', function(){
    it('should override the parent function area()', function(){
      assert.strictEqual("17.5", String(assign.area()));
    });
  });
});