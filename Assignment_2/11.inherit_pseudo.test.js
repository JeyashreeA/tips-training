/*global console:true,describe:true,it:true,require:true, module:true*/
var assert = require("assert");
var assign = require("./11.inherit_pseudo.js");

describe('inherit_pseudo.js', function(){
  describe('#inherit_pseudo()', function(){
    it('should override the parent function draw()', function(){
      //assert.deepEqual([0,1,1,2], assign.fibonacci_output(4));
      var value = assign.draw();
      assert(value === "drawing a square");
    });
  });
});

describe('inherit_pseudo.js', function(){
  describe('#get_angle()', function(){
    it('should override the parent function get_angle()', function(){
      //assert.deepEqual([0,1,1,2], assign.fibonacci_output(4));
      var value = assign.get_angle();
      assert(value === "angle is usually acute");
    });
  });
});