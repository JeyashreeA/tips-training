/*global console:true*/
a = 7;

var arith_obj = {
    "a" : 5,
    "add" : function(b) {               //called lexical scoping(in terms of this being a func and not a method
        var that = this;
        var c;
        var doAdd = function () {
        c = that.a+b;
        };
        doAdd();
        return c;
    },
    
    "subtract" : function(a,b) {
        return a-b;
    },
    
    "multiply" : function(b) {
        var c;
        var doMultiply = function () {
        c = this.a*b;          //a is undefined, doesnt bind to the global a as suggested in the books (works with alert)
                                //edit: works with console if we declare a without var -->assigns to global scope (why?)
        };
        doMultiply();
        return c;
    },
    
    "divide" : function(a,b) {
        return (a/b);
    }
};

console.log(arith_obj.add(5)); //function invocation
console.log(arith_obj.multiply(5));

console.log(arith_obj.subtract(2,3));  //method invocation

//var value = 500; //Global variable
//var obj = {
//    value: 0,
//    increment: function() {
//        this.value++;
//
//        var innerFunction = function() {
//            console.log(this.value);
//        }
//
//        innerFunction(); //Function invocation pattern
//    }
//}
//obj.increment();
