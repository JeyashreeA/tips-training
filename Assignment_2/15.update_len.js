/*global console:true,describe:true,it:true,require:true, module:true*/
var interest = ["books", "music", "travel", "fun", "food"];

console.log(interest);

console.log(interest.length);

interest[6]="sleep";

console.log(interest);

console.log(interest.length);

interest.length = 9;

console.log(interest);

console.log(interest.length);

//We can easily update the length of the array. All we have to do is..either use the length prop and set it to a new value...
//in this case null values are added. Another option is making use of a simple [] notation and change the value.