/*global console:true,module:true*/
var lineFunction = {
    "length" : 4,
    "name" : "line",
    "get_length" : function() {
    return this.length;
    },
    "draw" : function() {
        console.log("drawing a shape");
    }  
};

//---------------------------------------------------------Square function------------------------------------------------------------//

var mySquare = Object.create(lineFunction);

mySquare.area = function(length) {
    return length * length;
};

mySquare.get_angle = function() {
    console.log("angle is 90");
};

mySquare.draw = function() {
        console.log("drawing a square");
};

mySquare.draw();
mySquare.get_angle();
console.log("Area: "+mySquare.area(5));
console.log("\n");

//---------------------------------------------------------Rectangle function------------------------------------------------------------//

var myRect = Object.create(lineFunction);

myRect.area = function(length,breadth) {
    return length * breadth;
};

myRect.breadth = 5;

myRect.get_breadth = function() {
    return this.breadth;
};

myRect.draw = function() {
        console.log("drawing a rectangle");
};

myRect.draw();
console.log("Area: "+myRect.area(5,7));
//console.log(myRect.get_breadth());
console.log("\n");

//---------------------------------------------------------Rhombus function------------------------------------------------------------//

var myRhombus = Object.create(mySquare);

myRhombus.area = function(diag1,diag2) {
    var a = Number((diag1 * diag2)/2);
    console.log(a);
    return a;
};

myRhombus.get_angle = function() {
    console.log("angle is usually acute");
};

myRhombus.draw = function() {
        console.log("drawing a rhombus");
};

myRhombus.draw();
myRhombus.get_angle();
console.log("Area: "+myRhombus.area(5,7));
console.log("\n");

module.exports.draw = mySquare.draw;
module.exports.area = myRhombus.area;