/*global console:true,describe:true,require:true,process:true, module:true*/
var stack = [ 1, 2, 3, 4, 5 ], choice;
var arr= [];
var prompt = require('prompt');

prompt.start();
var stack_op = function() {
console.log("1.Push");
console.log("2.pop");
console.log("3.exit");
prompt.get(['choice'], function (err, res) {
    if(err) { return onErr(err); }
    else {
        choice = parseInt(res.choice);
        switch(choice) {
            case 1: push();
                break;
            case 2: pop();
                break;
            case 3: process.exit();
                break;
            default: console.log("enter a valid number");
                stack_op();
                break;
        }
    }
});
};
var push = function() {
    prompt.start();
    prompt.get(['number'], function (err, res) {
        if(err) { return onErr(err); }
        else {
            callPush(res.number);
        }
    });
};

function callPush(number) {
    arr.push(number);
    console.log(arr);
    stack_op();
    return arr;
}

function callPop() {
    arr.pop();
    console.log(arr);
    stack_op();
    return arr;
}

var pop = function() {
    try {
        if(arr.length === 0)
            throw "Empty array cant be popped-->push to pop";
          callPop();  
        
        }
    catch (err) {
        console.log(err);
        stack_op();
    }
    
};

function onErr(err) {
    console.log(err);
    return;
}

module.exports.push = callPush;
module.exports.pop = callPop;


stack_op();