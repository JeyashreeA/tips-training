/*global console:true,describe:true,it:true,require:true, module:true*/
var str = "Hello world!";

var arr = [1,2,3,4,5,6];

var res_str = str.slice(2,6);

var res_arr = arr.slice(2,3);

console.log("String is sliced as " +res_str);
console.log("array is sliced as " +res_arr);

var res_arr_sp = arr.splice(2,2);

console.log("array is spliced as " +res_arr_sp);

arr.splice(2,0,"ins","ins");

console.log("array is spliced as " +arr);



//slice is available in both string and array. The parameters to be passed are the starting and the ending index that we want to slice.
//splice is available only for arrays. The parameters to be passed are the starting index and the length upto which we want to splice.
//Using splice we can both add and remove values from an array