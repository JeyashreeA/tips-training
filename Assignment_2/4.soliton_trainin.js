var solitonTrainingRoom = {
    "table" : "huge",
    "vase" : "glass",
    "chairs" : "red",
    "board" : "white",
    "soft_boards" : "filled",
    "fan" : "off",
    "AC" : "on",
    "computers" : "on",
    "people" : "working"
};