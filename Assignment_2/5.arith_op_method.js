/*global console:true*/
var arith_obj = {
    "a" : 5,
    "add" : function(b) {
        return this.a+b;
    },
    "subtract" : function(a,b) {
        return a-b;
    },
    "multiply" : function(a,b) {
        return this.a*b;
    },
    "divide" : function(a,b) {
        return (a/b);
    }
};

console.log(arith_obj.add(2,3));
console.log(arith_obj.add(3));
console.log(arith_obj.subtract(7,3));
console.log(arith_obj.multiply(2,3));
console.log(arith_obj.divide(2,3));
