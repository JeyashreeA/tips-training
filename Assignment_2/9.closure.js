/*global console:true*/
//closures basically indicate a scenario in which a func will return a func... the main thing is that values passed to the closure function
//are retained within the returned function


function add(a) {                     //closure function
    return function (b) {             //function returned by closure
        console.log(a+b);
    };
}

var arith_obj = {
    "a" : 5,
    "subtract" : function(b) {           //closure function inside object
        return function (a) {
                    return a-b;
        };
    }
};

function subtract(a) {
    return function (b) {
        console.log(a-b);
    };
}

function multiply(a) {
    return function (b) {
        console.log(a*b);
    };
}

function divide(a) {
    return function (b) {
        console.log(a/b);
    };
}

var add_out = add(3);

add_out(4);            //calling a closure function

var sub_out = arith_obj.subtract(3);

console.log(sub_out(6));