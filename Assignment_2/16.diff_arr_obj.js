/*global console:true,describe:true,it:true,require:true, module:true*/
var arr = new Array();
arr["a"] = 1; //assigns to an object rather than to arr
arr = [1,2,3];
console.log(arr);
console.log("the type of arr is: "+typeof(arr));

var obj = new Object();

console.log("the type of obj is: "+typeof(obj));

//Both are objects as everything in javascript is, but the difference comes in the fact that object's values are assigned using keys and
//arrays use the index to find the values