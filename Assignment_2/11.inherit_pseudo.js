/*global console:true,describe:true,it:true,require:true, module:true*/
var shapeFunction = function() {
    
};

shapeFunction.prototype.draw = function() {
        console.log("drawing a shape");
    };

//---------------------------------------------------------Line function------------------------------------------------------------//

var lineFunction = function(length) {
    this.length = length;
    this.name = "line";
};

lineFunction.prototype.get_length = function() {
    return this.length;
};

//var line = new lineFunction(4);

//console.log(line.get_length());

//---------------------------------------------------------Square function------------------------------------------------------------//

var squareFunction = function(length) {
    this.length = length;
    this.name = "square";
};

squareFunction.prototype = new lineFunction();

squareFunction.prototype.area = function() {
    return this.length * this.length;
};

squareFunction.prototype.get_angle = function() {
    console.log("angle is 90");
};

var mySquare = new squareFunction(7);
mySquare.prototype = new shapeFunction();
mySquare.prototype.draw = function() {
        console.log("drawing a square");
    return "drawing a square";
};
mySquare.prototype.draw();
mySquare.get_angle();
console.log("Area: " + mySquare.area());
console.log("\n");


//---------------------------------------------------------Rectangle function------------------------------------------------------------//

var rectFunction = function(length, breadth) {
    this.length = length;
    this.breadth = breadth;
    this.angle = 90;
};

rectFunction.prototype = new lineFunction();

rectFunction.prototype.area = function() {
    return this.length * this.breadth;
};

rectFunction.prototype.get_breadth = function() {
    return this.breadth;
};

rectFunction.prototype.draw = function() {
    console.log("drawing a rectangle");
};

var rect = new rectFunction(4,9);

rect.draw();
console.log("Area: " + rect.area());
console.log("\n");

//---------------------------------------------------------Rhombus function------------------------------------------------------------//

var rhombusFunction = function(diag1, diag2) {
    this.diag1 = diag1;
    this.diag2 = diag2;
    this.name = "rhombus";
    this.angle = 60;
};

rhombusFunction.prototype = new squareFunction();

rhombusFunction.prototype.draw = function() {
    console.log("drawing a rhombus");
};

rhombusFunction.prototype.get_angle = function() {
    console.log("angle is usually acute");
    return "angle is usually acute";
};

rhombusFunction.prototype.area = function() {
    return ((this.diag1 * this.diag2)/2);
};

var rhombus = new rhombusFunction(4,9);

rhombus.draw();
rhombus.get_angle();
console.log("Area: " + rhombus.area());
console.log("\n");


module.exports.draw = mySquare.prototype.draw;
module.exports.get_angle = rhombus.get_angle;