/*global console:true*/
var i;
var camera = {
    "model_name" : "Nikon",
    "shutter" : "open",
    "pixels" : 200,
    "toggleShutter" : function () {
        this.shutter = this.shutter?false:true;
    }
};

for (i in camera) {
    console.log("camera."+ i + ":" + camera[i]);
}

console.log("listing using keys function");
console.log(Object.keys(camera));  //returns an array having the property names(only its own properties)
console.log(Object.getOwnPropertyNames(camera));  //doesnt matter if it is not enumerable


//function listAllProperties(o){ 
//    
//	var objectToInspect;     
//	var result = [];
//	
//	for(objectToInspect = o; objectToInspect !== null; objectToInspect = Object.getPrototypeOf(objectToInspect)){  
//        console.log("loop");
//		result = result.concat(Object.getOwnPropertyNames(objectToInspect));  
//	}
//	
//	return result; 
//}
////var camera = new camera;
//
//var result = listAllProperties(camera)
//console.log(result);