/*global console:true*/
var i;
var camera = {
    "model_name" : "Nikon",
    "shutter" : "open",
    "pixels" : 200,
    "toggleShutter" : function () {
        this.shutter = this.shutter?false:true;
    }
};

for (i in camera) {
    if(typeof (camera[i]) === "number") {
        console.log("camera."+ i + ":" + camera[i]);
}
}