/*global require:true,describe:true,it:true*/
var assert = require("assert");
var assign = require("./Assignment_1.js");

describe('Assignment_1.js', function(){
  describe('#Fibonacci()', function(){
    it('should return the fibonacci series', function(){
      assert.deepEqual([0,1,1,2], assign.fibonacci_output(4));
      var value = assign.fibonacci_output(-1);
      assert.throws ( function() {
                        throw new Error("Wrong value");
                        }, value);
    });
  });
});

describe('Assignment_1.js', function(){
  describe('#String()', function(){
    it('should return string without vowels and spaces', function(){
      assert.deepEqual("Hmjyshr", assign.string_output("Hi i am jeyashree"));
    });
  });
});

describe('Assignment_1.js', function(){
  describe('#object()', function(){
    it('should return the required book', function(){
        var searched = [{
                            "name": 'J.K.Rowling', 
                            "book": ['harry potter series', 'cuckoo calling']
                        }];
        var result = assign.search_book("har");
      assert.deepEqual(searched, result);
    });
  });
});

describe('Assignment_1.js', function(){
  describe('#Array()', function(){
    it('should return the sorted array', function(){
      assert.deepEqual(
          [ {"id":4, "name": 'Arjun', "age": 22},
            {"id":16, "name": 'Jeyashree', "age": 21},
            {"id":25, "name": 'Manu', "age": 21},
            {"id":46, "name": 'Sangeetha', "age": 21}
        ], assign.array_output("id"));
    });
  });
});