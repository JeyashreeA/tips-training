var utils = require("./util_modules.js");
var checkList = {
 num : 5,
 str : "Hello",
 bool : true,
 func : function (){
    console.log("hello");
},
 arr : [],
 obj : {}
};

//console.log(checkList);
var i;

checkList.func.prototype = new Error();
var arr = new checkList.func;

for(i in checkList) {
    
    console.log(i+":"+checkList[i]+" is undefined?: "+utils.isUndefined(checkList[i]));
    console.log(i+":"+checkList[i]+" is Null?: "+utils.isNull(checkList[i]));
    console.log(i+":"+checkList[i]+" is NaN?: "+utils.isNaN(checkList[i]));
    console.log(i+":"+checkList[i]+" is Array?: "+utils.isArray(checkList[i]));
    console.log(i+":"+checkList[i]+" is object?: "+utils.isObject(checkList[i]));
    console.log(i+":"+checkList[i]+" is String?: "+utils.isString(checkList[i]));
    console.log(i+":"+checkList[i]+" is Number?: "+utils.isNumber(checkList[i]));
    console.log(i+":"+checkList[i]+" is function?: "+utils.isFunction(checkList[i]));
    console.log(i+":"+checkList[i]+" is boolean?: "+utils.isBoolean(checkList[i]));
    console.log(i+":"+checkList[i]+" is error?: "+utils.isError(checkList[i]));
    console.log("\n");
}

console.log(arr+" inherits from error?: "+utils.isError(arr));
//console.log(arr+" inherits from error?: "+utils.isNull(""));
