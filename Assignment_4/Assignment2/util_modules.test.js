/*global console:true,describe:true,it:true,require:true*/
var assert = require("assert");
var assign = require("./util_modules.js");

describe('util_modules.js', function(){
  describe('#isNaN()', function(){
    it('Check if the value passed is NaN', function(){
      assert.equal(false, assign.isNaN(5));
      assert.equal(true, assign.isNaN("hello"));
    });
  });
});
    
describe('util_modules.js', function(){
  describe('#isNull()', function(){
    it('Check if the value passed is Null', function(){
      assert.equal(true, assign.isNull(null));
      assert.equal(false, assign.isNull(""));
      assert.equal(false, assign.isNull("hello"));
    });
  });
});
        
describe('util_modules.js', function(){
  describe('#isUndefined()', function(){
    it('Check if the value passed is undefined', function(){
      var a;
      var b = "";
      assert.equal(false, assign.isUndefined(b));
      assert.equal(true, assign.isUndefined(a));
    });
  });
});
    
describe('util_modules.js', function(){
  describe('#isError()', function(){
    it('Check if the value passed inherits from Error object', function(){
      assert.equal(false, assign.isError("hello"));
      assert.equal(false, assign.isError(Error));
    });
  });
});

describe('util_modules.js', function(){
  describe('#isBoolean()', function(){
    it('Check if the value passed is boolean', function(){
      var a = false;
      assert.equal(false, assign.isBoolean(a));
      assert.equal(true, assign.isBoolean(true));
    });
  });
});
    
describe('util_modules.js', function(){
  describe('#isNumber()', function(){
    it('Check if the value passed is NaN', function(){
      assert.equal(false, assign.isNumber("hello"));
      assert.equal(true, assign.isNumber(7.7));
      assert.equal(true, assign.isNumber(6));
    });
  });
});
    
describe('util_modules.js', function(){
  describe('#isString()', function(){
    it('Check if the value passed is String', function(){
      assert.equal(false, assign.isString(5));
      assert.equal(true, assign.isString("hello"));
    });
  });
});
    
describe('util_modules.js', function(){
  describe('#isArray()', function(){
    it('Check if the value passed is Array', function(){
      var a = [];
      var b = {};
      assert.equal(false, assign.isArray(b));
      assert.equal(true, assign.isArray(a));
    });
  });
});
    
describe('util_modules.js', function(){
  describe('#isObject()', function(){
    it('Check if the value passed is Object', function(){
      var a = [];
      var b = 8;
      assert.equal(false, assign.isObject(b));
      assert.equal(true, assign.isObject(a));
    });
  });
});
    
describe('util_modules.js', function(){
  describe('#isFunction()', function(){
    it('Check if the value passed is function', function(){
      var func = function() {};
      assert.equal(false, assign.isNaN(5));
      assert.equal(true, assign.isNaN(func));
      assert.equal(true, assign.isNaN(Number));
    });
  });
});