function isUndefined(value) {
    if( value === undefined && typeof value === "undefined" ) {
        return true;
    }
    return false;
}

function isNull(object) {
    if(object === null && typeof object === "object") {
        return true;
    }
    return false;
}

function isNaN(object) {
    if(parseFloat(object) && object != 0 && typeof object === "number") {
        return false;
    }
    return true;
}

function isArray(object) {
    //console.log(object)
    if(typeof object === "object" && object.hasOwnProperty('length')) {
        return true;
    }
    return false;
}

function isObject(object) {
    //console.log(object)
    if(typeof object === "object") {
        return true;
    }
    return false;
}

function isFunction(object) {
    //console.log(object)
    if(typeof object === "function") {
        return true;
    }
    return false;
}

function isNumber(object) {
    //console.log(object)
    if(typeof object === "number") {
        return true;
    }
    return false;
}

function isString(object) {
    //console.log(object)
    if(typeof object === "string") {
        return true;
    }
    return false;
}

function isBoolean(object) {
    //console.log(object)
    if(object && typeof object === "boolean") {
        return true;
    }
    return false;
}

function isError(object) {
    if( object instanceof Error) {
        return true;
}
return false;
}

//var a = function() {
//    this.a1=6;
//}
//a.prototype = new Error();
//var arr = new a;
//console.log(a.prototype);
////console.log(isError(6));
a = NaN;
console.log(typeof(a));

module.exports.isArray = isArray;
module.exports.isObject = isObject;
module.exports.isNumber = isNumber;
module.exports.isString = isString;
module.exports.isFunction = isFunction;
module.exports.isError = isError;
module.exports.isBoolean = isBoolean;
module.exports.isNaN = isNaN;
module.exports.isNull = isNull;
module.exports.isUndefined = isUndefined;
