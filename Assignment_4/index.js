#! /usr/bin/env node
var date = new Date();

console.log(" The date is : "+date.toDateString());
console.log(" The time is : "+date.toLocaleTimeString());

var str = date.toString();
var index = str.indexOf("(");
var timeZone = str.substring((index+1),(str.length-1));
console.log(" The current date and timezone is : "+timeZone);

console.log(" The calendar of the current month is ");

var Table = require('cli-table');
var table = new Table({
    head: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
    colWidths: [5, 5, 5, 5, 5, 5, 5]
});


var arr = [];
var i, j, k;
var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
var startDay = firstDay.getDay();
var endDate = lastDay.getDate();

for (i = 0; i < startDay; i++) {
    arr.push(" ");
}
for (j = 1; j <= 7-startDay; j++) {
    arr.push(j);
    if(arr.length === 7) {
        table.push(arr);
        arr = [];
    }
}

for (k = j; k <= endDate; k++) {
    arr.push(k);
    if(arr.length === 7) {
        table.push(arr);
        arr = [];
    }
}
table.push(arr);
 
console.log(table.toString());
