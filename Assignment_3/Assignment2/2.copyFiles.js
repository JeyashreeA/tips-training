/*global console:true,describe:true,it:true,require:true, module:true*/
var fs = require("fs");
var path = require ("path");
var arr = [], str, folder, location, i;

fs.readFile("config.txt" ,function(err, data) {
    if(err) { return onErr(err); }
    str = String(data);
    arr = str.split(/\r?\n/);
    location = arr.indexOf("Destination");
    for (i = 1; i < arr.length/2; i++) {
        (function (j) {
        arr[j] = path.normalize(arr[j]);
        arr[location+j] = path.normalize(arr[location+j]);
            //console.log("here "+arr[location+j]);
            fs.exists(arr[location+j], function (exist) {
                //console.log("exist"+exist+" "+arr[location+j]);
                if (exist) {
                    //console.log("true" + exist)
                    var rd = fs.createReadStream(arr[j]);
                    var wd = fs.createWriteStream(arr[location+j]);
                    rd.pipe(wd);
                    console.log("Copied a file "+path.basename(arr[j])+" to "+path.basename(arr[location+j])+"\n");
                }
                else {
                    //console.log("there "+arr[location+j]);
                    folder = path.dirname(arr[location+j]);
                    fs.mkdir(folder, function() {
                        console.log("Created a new folder: " +folder+"\n");
                        var rd = fs.createReadStream(arr[j]);
                        var wd = fs.createWriteStream(arr[location+j]);
                        rd.pipe(wd);
                        console.log("Copied a file "+path.basename(arr[j])+" to "+path.basename(arr[location+j]));
                    });
                }
            });
        })(i);
    }
});

function onErr(err) {
    console.log(err);
    return;
}