var fs = require("fs");
var path = require ("path");
var arr = [], str, folder, hrStart, hrend;

fs.readFile("config.txt" ,function(err, data) {
    if(err) { return onErr(err); }
    str = String(data);
    arr = str.split(/\r?\n/);
    location = arr.indexOf("Destination");
    console.log(arr);
    for (i = 1; i < arr.length/2; i++) {
        (function (j) {
        arr[j] = path.normalize(arr[j]);
        arr[location+j] = path.normalize(arr[location+j]);
            //console.log("here "+arr[location+j]);
            fs.exists(arr[location+j], function (exist) {
                //console.log("exist"+exist+" "+arr[location+j]);
                if (exist) {
                    hrStart = process.hrtime();
                    var rd = fs.createReadStream(arr[j]);
                    var wd = fs.createWriteStream(arr[location+j]);
                    rd.pipe(wd);
                    console.log("Copied a file "+path.basename(arr[j])+" to "+path.basename(arr[location+j])+"\n");
//                    fs.watchFile(arr[location+j], function (curr, prev) {
//  console.log('the current mtime is: ' + curr.mtime.getTime() + "for" + path.basename(arr[location+j])+"\n");
//  console.log('the previous mtime was: ' + prev.mtime.getTime() + "for" + path.basename(arr[location+j])+"\n");
//                        copyTime = curr.mtime.getTime() - prev.mtime.getTime();
                    hrend = process.hrtime(hrStart);
                        fs.appendFile("config.txt","\nThe time taken to copy "+path.basename(arr[location+j])+" is "+hrend[1]/1000000+" milliseconds", function(err) {
                            if(err) {return onErr(err); }
                            console.log("written");
                        });
//});
                }
                else {
                    //console.log("there "+arr[location+j]);
                    folder = path.dirname(arr[location+j]);
                    hrStart = process.hrtime();
                    fs.mkdir(folder, function() {
                        console.log("Created a new folder: " +folder+"\n");
                        var rd = fs.createReadStream(arr[j]);
                        var wd = fs.createWriteStream(arr[location+j]);
                        rd.pipe(wd);
                        console.log("Created a file at "+folder+" named "+path.basename(arr[location+j]));
                        hrend = process.hrtime(hrStart);
                        fs.appendFile("config.txt","\nThe time taken to copy "+path.basename(arr[location+j])+" is "+hrend[1]/1000000+" milliseconds", function(err) {
                            if(err) {return onErr(err); }
                            console.log("written");
                        });
                    });
                }
            });
        })(i)
    };
});

function onErr(err) {
    console.log(err);
    return;
};