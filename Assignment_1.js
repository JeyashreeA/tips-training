/* global console:true,require:true,process:true,module:true */
var i, j, k, choice, option, radius, length, space = 1, name, temp, string = [], book = [], index, test_arr = [];
var student_arr = [ {"id":16, "name": 'Jeyashree', "age": 21},
                    {"id":4, "name": 'Arjun', "age": 22},
                    {"id":46, "name": 'Sangeetha', "age": 21},
                    {"id":25, "name": 'Manu', "age": 21}
                ];
var book_arr = [ {"name": 'J.K.Rowling', "book": ['harry potter series', 'cuckoo calling']},
                 {"name": 'Chetan Bhagat', "book": ['asd','zxc']},
                 {"name": 'XYZ', "book": ['qwe','edr']},
                 {"name": 'ABC', "book": ['jey','shr']}
                ];
var search_list = [];
var prompt = require('prompt');


function displayList(){
    console.log("\n");
    console.log("1.Diamond pattern");
    console.log("2.fibonacci series");
    console.log("3.string display");
    console.log("4.sort_array");
    console.log("5.book_object");
    prompt.start();
    prompt.get(['choice'], function(err, result) {
        if(err) { return onErr(err); }
        else { choose(result); }
    });
}

function choose(result) {
    choice = parseInt(result.choice);
    switch (choice) {
        case 1: diamond_pattern();
            break;
        case 2: fibonacci();
            break;
        case 3: string_display();
            break;
        case 4: sort_array();
            break;
        case 5: book_object();
            break;
//        case 6: exit
        default: console.log("Enter a valid choice\n");
                 displayList();
    }
}
                    
function diamond_pattern() {
  prompt.start();

  prompt.get(['radius'], function (err, result) {
    if (err) { return onErr(err); }
      else { return diamond_output(result.radius); }
   });
}

function fibonacci() {
    prompt.start();

    prompt.get(['length'], function (err, result) {
    if (err) { return onErr(err); }
      else { return fibonacci_output(result.length); }
   });
}

function string_display() {
    prompt.start();

    prompt.get(['string'], function (err, result) {
    if (err) { return onErr(err); }
      else { return string_output(result.string); }
   });
}

function sort_array() {
    prompt.start();

    prompt.get(['sort_by'], function (err, result) {
    if (err) { return onErr(err); }
      else { return array_output(result.sort_by); }
   });
}

function book_object() {
    prompt.start();
    console.log("\n");
    console.log("1.Add Author");
    console.log("2.Add book");
    console.log("3.search for a book");
    prompt.get(['choose'], function (err, result) {
    if (err) { return onErr(err); }
      else { return object_output(result); }
   });
}

var isInteger = function(value) {
    return (Math.floor(value) === value);
};

function diamond_output(radiusIn) {
    radiusIn = Number(radiusIn);
    space = Math.round(radiusIn/2)-1;
    try {
        radius = radiusIn % 2;
        if (isNaN(radiusIn) || !isInteger(radiusIn))
            throw "is not a number";
        if ( radius === 0 )
            throw "even numbers are not allowed";
        if (radius < 0)
            throw "number cant be negative";
        
    }
    catch(error) {
        console.log(error);
        displayList();
        return;
    }
     
    for ( i = 1; i <= radiusIn; i += 2) {
      for ( j = 0; j < space; j++)
        process.stdout.write(" ");

      for ( j = 0; j < i; j++)
        process.stdout.write("*");

      process.stdout.write("\n");
            space --;
    }
    space = 1;
    for ( i = radiusIn-2; i > 0; i -= 2) {
      for ( j = 0; j < space; j++)
        process.stdout.write(" ");

      for ( j = 0; j < i; j++)
        process.stdout.write("*");

      process.stdout.write("\n");
            space ++;
    }
    return radiusIn;
}



function fibonacci_output(length) {
    try {
        length = Number(length);
        if( length < 0 )
            throw "negative numbers not allowed";
        if (typeof(length) !== "number" || !isInteger(length))
            throw "is not a number";
    }
    catch (err) {
        console.log(err);
        displayList();
        return;
    }
    length = length-2;
    console.log("The fibonacci series is");
    i = 0;
    j = 1;
    console.log(i);
    console.log(j);
    test_arr.push(0);
    test_arr.push(1);
    while (length !== 0) {
        k = i + j;
        console.log(k);
        test_arr.push(k);
        i = j;
        j = k;
        length--;
    }
    
    return test_arr;
}

function string_output(stringIn) {
    try {
        length = stringIn.length;
        if(length === 0)
            throw "invalid length of string";
    }
    catch(err) {
        console.log(err);
        displayList();
        return;
    }
    string = stringIn.split("");
    for (i = 0; i < length; i++) {
        if(string[i] === ' ' || string[i] === 'a' || string[i] === 'e' || string[i] === 'i' || string[i] === 'o' || string[i] === 'u') {
            string.splice(i,1);
            i--;
        }
    }
    console.log("\nThe string with all vowels and spaces removed is:");
    console.log(string.join(""));
    return string.join("");
}


function array_output(sort_by) {
    try {
        if(sort_by != "id")
            throw "Can sort only by id";
    }
    catch (err) {
        console.log(err);
        displayList();
        return;
    }
    console.log("Initial list of objects:");
    console.log("\n");
    for(i = 0; i < student_arr.length; i++) {
        console.log("details of student "+ (i+1));
        console.log("id: "+student_arr[i].id);
        console.log("name: "+student_arr[i].name);
        console.log("age: "+student_arr[i].age);
        console.log("\n");
    }
    for (i = 0; i < student_arr.length; i++) {
        for (j = i+1; j < student_arr.length; j++) {
            if (student_arr[i].id > student_arr[j].id) {
                temp = student_arr[j];
                student_arr[j] = student_arr[i];
                student_arr[i] = temp;
            }
        }
    }
    console.log("The sorted list is:");
    console.log("\n");
    for(i = 0; i < student_arr.length; i++) {
        console.log("details of student "+ (i+1));
        console.log("id: "+student_arr[i].id);
        console.log("name: "+student_arr[i].name);
        console.log("age: "+student_arr[i].age);
        console.log("\n");
    }
    return student_arr;
}

function object_output(result) {
    
    option = parseInt(result.choose);
    switch(option) {
        case 1: add_author();
            break;
        case 2: add_author();
            break;
        case 3: search_book();
            break;
        default: console.log("enter a valid choice!!");
    return ;
    }
}

function add_author() {
    prompt.start();
    console.log("Enter the author name");
    prompt.get(['name'], function (err, result) {
    if (err) { return onErr(err); }
      else { 
          for (i = 0; i < book_arr.length; i++) {
            if(book_arr[i].name === result.name){
              add_book(result.name);
                break;
            }
              }
            if(i === book_arr.length) {
                name = result.name;
                add_book(name);

            }
          
      }
   });
}

function add_book(name) {
    console.log("enter one of the books");
    prompt.start();
    prompt.get(['book'], function (err, result) {
      if(err) { return onErr(err); }
      else {
          try {
              console.log(typeof (result.book));
              if( typeof (result.book) !== "string" )
                  throw "Not a valid input!! enter a book name";
            }
          catch(error) {
              console.log(error);
              displayList();
              return;
          } 
          for (i = 0; i < book_arr.length; i++) {
              //console.log("coming in");
              j = (i+1);
            if(book_arr[i].name === name) {
                //console.log("if");
                book_arr[i].book.push(result.book);
                break;
            }
            if (j === book_arr.length){
                //console.log("pushing");
                book_arr.push({'name': name, 'book':[result.book]});
                break;
            }
            }
     //console.log(book_arr);   
    console.log("The books available are:");
    console.log("\n");
    for(i = 0; i < book_arr.length; i++) {
        console.log("details of book"+ (i+1));
        console.log("\n");
        console.log("Author Name: "+book_arr[i].name);
        console.log("\tbook: "+book_arr[i].book);
        console.log("\n");
    }
      }
    });  
}

function search_book() {
    console.log("enter atleast a part of the book's name");
    prompt.start();
    prompt.get(['book'], function (err, result) {
        if(err) { return onErr(err); }
        else {
            return searchBookFunction(result.book);
        }
    });
}

function searchBookFunction (bookIn) {
    for(i = 0; i < book_arr.length; i++) {
                //console.log("coming in");
                for(j = 0; j < book_arr[i].book.length; j++) {
                    //console.log(book_arr[i].book[j]);
                    index = book_arr[i].book[j].indexOf(bookIn);
                    //console.log(index);
                    if(index != -1) {
                        search_list.push(book_arr[i]);
                    }
                }
            }
    console.log("The books available are:");
    console.log("\n");
    for(i = 0; i < search_list.length; i++) {
        console.log("details of book"+ (i+1));
        console.log("\n");
        console.log("Author Name: "+search_list[i].name);
        console.log("\tbook: "+search_list[i].book);
        console.log("\n");
    }
    return search_list;
}

function onErr(err) {
    console.log(err);
    return 1;
}

displayList();

module.exports.diamond_output = diamond_output;
module.exports.fibonacci_output = fibonacci_output;
module.exports.string_output = string_output;
module.exports.array_output = array_output;
module.exports.add_author = add_author;
module.exports.add_book = add_book;
module.exports.search_book = searchBookFunction;