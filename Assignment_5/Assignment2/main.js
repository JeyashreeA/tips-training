/* global console:true, require:true, process:true*/

var spawn = require('child_process').spawn;
var q = require('q');
var child_1Play, child_2Play, setChild_1 = false, setChild_2 = false;

function getDataFirst () {

    var defered = q.defer();
    child_1 = spawn('node', ['child_1.js']);
    child_1.stdout.on('data', function (data) {
        child_1Play = data.toString();

        console.log("child1"+child_1Play);
        setChild_1 = true;
        defered.resolve();
    });
    return defered.promise;
}


function resolveFunc() {
    var defered = q.defer();
  
    setTimeout(function() {
   if(setChild_1 && setChild_2) {

        setChild_1 = false;
        setChild_2 = false;
        defered.resolve();
   
    }},2000);
    return defered.promise;
}


function thenResolve() {
    
    switch(child_1Play) {
        case "rock":
            if(child_2Play === "scissors") {
                console.log("Player one wins");
                
            }
            else if(child_2Play === "paper") {
                console.log("Player two wins");
                
            }
            else 
                console.log("Match draw");
            break;
            
        case "scissors":
            if (child_2Play === "rock") {
                console.log("Player two wins");
                
            }
            else if(child_2Play === "paper") {
                console.log("Player one wins");
                
            }
             else 
                console.log("Match draw");
            
        break;
            
        case "paper":
            if (child_2Play === "rock") {
                console.log("Player one wins");
                
            }
            else if(child_2Play === "scissors") {
                console.log("Player two wins");
                
            }
             else 
                console.log("Match draw");
        break;
        default: console.log("Something wrong");
            break;
    }

};

  setInterval(function () {
        
    console.log("\n");
    getDataFirst().then(function() {
    
    child_2 = spawn('node', ['child_2.js']);
    child_2.stdout.on('data', function (data) {
        child_2Play = data.toString();
       
        console.log("child2"+child_2Play);
        setChild_2 = true;
        resolveFunc().then(thenResolve());
    });
});
}, 2000);
    
