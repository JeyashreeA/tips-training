/*global console:true,describe:true,it:true,require:true, module:true*/
var http = require("http");
var fs = require("fs");
var q = require("q");
var defered, length, total, currentData = 0;

var file = fs.createWriteStream("Downloads\\node.exe");
downloadFile().then(function () {
    console.log('node.exe downloaded!!');
}, function() {
    console.log('Something is wrong!!');
});

function downloadFile () {
    defered = q.defer();
    http.get("http://nodejs.org/dist/v0.12.4/node.exe", function(res) {
        length = parseInt(res.headers[ 'content-length' ], 10 );
        total = length / 1048576;
        res.on('data', function(data) {
            currentData += data.length;
            if(currentData%1000 === 0)
                console.log("Progress: Downloading ---"+((currentData/length)*100).toFixed(2)+"%       Total size: "+total.toFixed(2)+"mb" );
            file.write(data);
        }).on('end', function() {
            file.end();
            return defered.resolve();
            
        }).on('error', function() {
            return defered.reject();
        });
    });
    return defered.promise;
}
    